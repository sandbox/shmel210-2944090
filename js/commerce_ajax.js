/**
 * @file
 * Defines Javascript behaviors for the commerce cart module
 */

(function ($, Drupal) {
  'use strict';
  // num-spinner
  var body = $('body');
  body.on('change', '.cart-block__item [name="quantity"]', function(){
    SpinnerAjaxUpdate(this);
  });
  function SpinnerAjaxUpdate(elem){
    var $form = $(elem).closest('form');
    var $cart_block = $('.cart-block');
    var $cart_summary = $('.cart-block--summary');
    var form_data = new FormData($form[0]);
    $.ajax({
      method: 'POST',
      processData: false,
      contentType: false,
      url: '/update-quantity',
      data: form_data,
      success: function (html) {
        $cart_block.replaceWith($(html).find('.cart-block'));
        $cart_summary.replaceWith($(html).find('.cart-block--summary'));
      },
      complete: function () {}
    });
  }

  // delete product
  body.on('click', '.cart-block__remove', function() {
    var $form = $(this).closest('form');
    var form_data = new FormData($form[0]);
    var $cart_block = $('.cart-block');
    var $cart_summary = $('.cart-block--summary');
    $.ajax({
      method: 'POST',
      processData: false,
      contentType: false,
      url: '/delete-product',
      data: form_data,
      success: function (html) {
        $cart_block.replaceWith($(html).find('.cart-block'));
        $cart_summary.replaceWith($(html).find('.cart-block--summary'));
      },
      complete: function () {
        AddClass_empty();
      }
    });
  });
  Drupal.behaviors.commerceCartBlock = {
    attach: function (context) {
      var $context = $(context);
      var $cart = $context.find('.cart--cart-block');
      var $cartButton = $context.find('.cart-block--link__expand');
      var $cartContents = $cart.find('.cart-block--contents');

      if ($cartContents.length > 0) {
        // Expand the block when the link is clicked
        $cartButton.on('click+', function (e) {
          // Prevent it from going to the cart.
          e.preventDefault();
          // Get the shopping cart width + the offset to the left.
          var windowWidth = $(window).width();
          var cartWidth = $cartContents.width() + $cart.offset().left;
          // If the cart goes out of the viewport we should align it right.
          if (cartWidth > windowWidth) {
            $cartContents.addClass('is-outside-horizontal');
          }
          // Toggle the expanded class.
          $cartContents
              .toggleClass('cart-block--contents__expanded')
              .slideToggle();
        });
      }
      
    }
  };

  function AddClass_empty(){
    // cart in a modal
    var cart_in_modal = $('.modal.cart-block--contents'),
        cart_item = cart_in_modal.find('.cart-block__item');
    $(document).ajaxComplete(function() {
        if (cart_item.find('form').length==0){
            cart_in_modal.addClass('cart-is-empty');
        }else{
            cart_in_modal.removeClass('cart-is-empty');
        }
    });
  }
  
  // stylising num-spinner
  function Spinner(){
    var spinner_wrapper = $('.input-quantity'),
        spinner = $('.input-quantity input');
    if (spinner_wrapper.find('.quantity-nav').length=== 0){
      $('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter(spinner);
      spinner_wrapper.each(function() {
      var spinner = jQuery(this),
        input = spinner.find('input[type="number"]'),
        btnUp = spinner.find('.quantity-up'),
        btnDown = spinner.find('.quantity-down'),
        min = input.attr('min'),
        max = input.attr('max');

      btnUp.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue >= max) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue + 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
        SpinnerAjaxUpdate(this);
      });

      btnDown.click(function() {
        var oldValue = parseFloat(input.val());
        if (oldValue <= min) {
          var newVal = oldValue;
        } else {
          var newVal = oldValue - 1;
        }
        spinner.find("input").val(newVal);
        spinner.find("input").trigger("change");
        SpinnerAjaxUpdate(this);
      });

    });
    }
  }
  Spinner();
  AddClass_empty();
  $(document).ajaxComplete(function() {
    Spinner();
    AddClass_empty();
  });

})(jQuery, Drupal, drupalSettings);
