commerce ajax
===============

CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Using the module
  * Author

INTRODUCTION
------------
When a user changes items quantity in Cart popup,
this module refreshes total sums w/o refreshing a page.
In Cart popup outputs currency symbol, not it's code.
Automatically create user and login
Adds field notifyAdmins at order type - it used for email notification of admins about new order


REQUIREMENTS
------------
Drupal Commerce 2.x


INSTALLATION
------------
Install module as usual via Drupal UI, Drush or Composer.


USING THE MODULE
----------------
Delete standard cart block
Add block "Commerce ajax cart block"
Install my fork of module dc_ajax_add_cart from 
install  igrowl from 
add libraries unpack animate.tar and igrowl.tar from module dc_ajax_add_cart to /libraries
add edit price field at view commerce_products


To customize cart block markup see commerce_ajax/templates/commerce-ajax-cart-block.html.twig
To customize css see commerce_ajax/css/commerce_ajax.theme.css
To customize js see commerce_ajax/js/commerce_ajax.js
To customize block context see commerce_ajax/src/Plugin/Block/CartBlock.php build
To set admins for notification about new order go to /admin/commerce/config/order-types/default/edit and set field notifyAdmins "Send receipt to this list of admin emails"


AUTHOR
------
shmel210  
Drupal: (https://www.drupal.org/user/2600028)  
Email: shmel210@zina.com.ua

Company: Zina Design Studio
Website: (http://zina.com.ua)  
Drupal: (https://www.drupal.org/user/361734/)  
Email: info@zina.com.ua

Similar projects and how they are different
-------------------------------------------
