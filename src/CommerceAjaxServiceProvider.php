<?php

namespace Drupal\commerce_ajax;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Registers event subscribers for installed Commerce modules.
 */
class CommerceAjaxServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // We cannot use the module handler as the container is not yet compiled.
    // @see \Drupal\Core\DrupalKernel::compileContainer()
    $modules = $container->getParameter('container.modules');
    if (isset($modules['commerce_order'])) {
      $container->register('commerce_ajax.order_subscriber', 'Drupal\commerce_ajax\EventSubscriber\OrderEventSubscriber')
        ->addTag('event_subscriber')
        ->addArgument(new Reference('entity_type.manager'))
        ->addArgument(new Reference('commerce.mail_handler'))
        ->addArgument(new Reference('commerce_order.order_total_summary'));
    }
  }

}
