<?php

namespace Drupal\commerce_ajax\EventSubscriber;

use Drupal\commerce\MailHandlerInterface;
use Drupal\commerce_order\OrderTotalSummaryInterface;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\state_machine\Event\WorkflowTransitionEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OrderEventSubscriber implements EventSubscriberInterface {
  use StringTranslationTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The mail handler.
   *
   * @var \Drupal\commerce\MailHandlerInterface
   */
  protected $mailHandler;

  /**
   * The order total summary.
   *
   * @var \Drupal\commerce_order\OrderTotalSummaryInterface
   */
  protected $orderTotalSummary;

  /**
   * Constructs a new OrderEventSubscriber object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, MailHandlerInterface $mail_handler, OrderTotalSummaryInterface $order_total_summary) {
    $this->entityTypeManager = $entity_type_manager;
    $this->mailHandler = $mail_handler;
    $this->orderTotalSummary = $order_total_summary;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events = [
      'commerce_order.place.pre_transition' => ['onPlaceTransition', -100],
      'commerce_order.place.post_transition' => ['sendOrderReceiptToAdmin', -100]
    ];
    return $events;
  }

  public static function generatePassword($length) {
    // This variable contains the list of allowable characters for the
    // password. Note that the number 0 and the letter 'O' have been
    // removed to avoid confusion between the two. The same is true
    // of 'I', 1, and 'l'.
    $allowable_characters = '1234567890';

    // Zero-based count of characters in the allowable list:
    $len = strlen($allowable_characters) - 1;

    // Declare the password as a blank string.
    $pass = '';

    // Loop the number of times specified by $length.
    for ($i = 0; $i < $length; $i++) {
      do {
        // Find a secure random number within the range needed.
        $index = ord(Crypt::randomBytes(1));
      } while ($index > $len);

      // Each iteration, pick a random character from the
      // allowable string and append it to the password:
      $pass .= $allowable_characters[$index];
    }

    return $pass;
  }

  /**
   * Creates a user when an order is placed.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The transition event.
   */
  public function onPlaceTransition(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $account = $order->getCustomer();
    if($account->isAnonymous() === FALSE) {
      return;
    }
    $billing_profile = $order->getBillingProfile();
    $email = $order->getEmail();
    $password = self::generatePassword(6);
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    if($user = user_load_by_mail($email)){
    } else {
      $user = \Drupal\user\Entity\User::create();
      $user->setPassword($password);
      $user->enforceIsNew();
      $user->setEmail($email);
      $user->setUsername($email);
      $user->set("init", 'email');
      $user->set("langcode", $language);
      $user->set("preferred_langcode", $language);
      $user->set("preferred_admin_langcode", $language);
      $user->activate();
      $user->save();
      if (!\Drupal::config('user.settings')->get('verify_mail')) {
        _user_mail_notify('register_no_approval_required', $user);
      }
      user_login_finalize($user);
    }
    $billing_profile->setOwner($user);
    $billing_profile->save();
    $order->setCustomer($user);
  }

  /**
   * Sends an order receipt email.
   *
   * @param \Drupal\state_machine\Event\WorkflowTransitionEvent $event
   *   The event we subscribed to.
   */
  public function sendOrderReceiptToAdmin(WorkflowTransitionEvent $event) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $event->getEntity();
    $order_type_storage = $this->entityTypeManager->getStorage('commerce_order_type');
    /** @var \Drupal\commerce_order\Entity\OrderTypeInterface $order_type */
    $order_type = $order_type_storage->load($order->bundle());
    if (!$order_type->shouldSendReceipt()) {
      return;
    }
    $to = $order->getEmail();
    if (!$to) {
      // The email should not be empty, unless the order is malformed.
      return;
    }

    $subject = $this->t('Order #@number confirmed', ['@number' => $order->getOrderNumber()]);
    $body = [
      '#theme' => 'commerce_order_receipt',
      '#order_entity' => $order,
      '#totals' => $this->orderTotalSummary->buildTotals($order),
    ];
    if ($billing_profile = $order->getBillingProfile()) {
      $profile_view_builder = $this->entityTypeManager->getViewBuilder('profile');;
      $body['#billing_information'] = $profile_view_builder->view($billing_profile);
    }
    $params = [
      'id' => 'order_receipt',
      'from' => $order->getStore()->getEmail(),
      'bcc' => '',
      'order' => $order,
    ];
    $customer = $order->getCustomer();
    if ($customer->isAuthenticated()) {
      $params['langcode'] = $customer->getPreferredLangcode();
    }
    $mail_admins = explode( ', ', $order_type->getThirdPartySetting('commerce_ajax', 'notifyAdmins', ''));
    if(!$mail_admins[0]) {
      $mail_admins = [];
    }
    foreach ($mail_admins as $to) {
      $this->mailHandler->sendMail($to, $subject, $body, $params);
    }
  }
}
