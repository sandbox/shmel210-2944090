<?php

namespace Drupal\commerce_ajax\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\views\field\UncacheableFieldHandlerTrait;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form element for editing the order item quantity.
 *
 * @ViewsField("commerce_ajax_edit_price")
 */
class EditPrice extends FieldPluginBase {

  use UncacheableFieldHandlerTrait;

  /**
   * Constructs a new EditPrice object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  /**
   * {@inheritdoc}
   */
  public function clickSortable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $row, $field = NULL) {
    return '<!--form-item-' . $this->options['id'] . '--' . $row->index . '-->';
  }

  /**
   * Form constructor for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsForm(array &$form, FormStateInterface $form_state) {
    // Make sure we do not accidentally cache this form.
    $form['#cache']['max-age'] = 0;
    // The view is empty, abort.
    if (empty($this->view->result)) {
      return;
    }
    $form[$this->options['id']]['#tree'] = TRUE;
    foreach ($this->view->result as $row_index => $row) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation */
      $product_variation = $this->getEntity($row);

      $form[$this->options['id']][$row_index] = [
        '#type' => 'textfield',
        '#title' => $this->t('Price'),
        '#title_display' => 'invisible',
        '#default_value' => number_format($product_variation->getPrice()->getNumber(), 2, '.', ''),
        '#size' => 9,
        '#ajax' => [
          'callback' => 'Drupal\commerce_ajax\Plugin\views\field\EditPrice::viewsFormSubmit',
          'wrapper' => 'not-exist-element',
          'event' => 'change',
        ],
        '#suffix' => '<span class="currency">'.$product_variation->getPrice()->getCurrencyCode().'</span>',
      ];
    }
  }

  /**
   * Submit handler for the views form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function viewsFormSubmit(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    $view = reset($form_state->getBuildInfo()['args']);
    $price_list = $form_state->getValue($triggering_element['#parents'][0], []);
    foreach ($price_list as $row_index => $price) {
      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $product_variation */
      $view_result = $view->result[$row_index];
      $product_variation = $view_result->_relationship_entities['variations'];
      if ($product_variation->getPrice()->getNumber() != $price) {
        $product_variation->setPrice(new \Drupal\commerce_price\Price($price, $product_variation->getPrice()->getCurrencyCode()));
        $product_variation->save();
      }
    }
    return ['#markup' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    // Do nothing.
  }

}
