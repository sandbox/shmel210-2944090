<?php
namespace Drupal\commerce_ajax\Plugin\Block;
use Drupal\commerce_price\Price;
use Drupal\Core\Block\BlockBase;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Cart' block.
 *
 * @Block(
 *   id = "commerce_ajax_cart",
 *   admin_label = @Translation("Commerce ajax cart block"),
 *   module = "commerce_ajax"
 * )
 */
class CartBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * The currency storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $currencyStorage;

  /**
   * Constructs a new CartBlock.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CartProviderInterface $cart_provider, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->cartProvider = $cart_provider;
    $this->entityTypeManager = $entity_type_manager;
    $this->currencyStorage  = $entity_type_manager->getStorage('commerce_currency');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('commerce_cart.cart_provider'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Builds the cart block.
   *
   * @return array
   *   A render array.
   */
  public function build() {
    $cachable_metadata = new CacheableMetadata();
    $cachable_metadata->addCacheContexts(['user', 'session']);

    /** @var \Drupal\commerce_order\Entity\OrderInterface[] $carts */
    $carts = $this->cartProvider->getCarts();
    $carts = array_filter($carts, function ($cart) {
      /** @var \Drupal\commerce_order\Entity\OrderInterface $cart */
      // There is a chance the cart may have converted from a draft order, but
      // is still in session. Such as just completing check out. So we verify
      // that the cart is still a cart.
      return $cart->hasItems() && $cart->cart->value;
    });

    $count = 0;
    $cart_items = [];
    $total_price = '0.00';
    $checkout_url = '';
    $currency = 'UAH';
    if (!empty($carts)) {
      foreach ($carts as $cart_id => $cart) {
        foreach ($cart->getItems() as $order_item) {
          if (!$checkout_url) {
            $order_id = $order_item->getOrder()->id();
            $checkout_url = "/checkout/{$order_id}";
          }
          /** @var \Drupal\commerce_product\Entity\ProductVariation $product_variation */
          $product_variation = $order_item->getPurchasedEntity();
          if (!$product_variation) {
            continue;
          }
          $currency = $order_item->getAdjustedTotalPrice()->getCurrencyCode();
          $currency = $this->currencyStorage->load($currency)->getSymbol();
          $count += (int) $order_item->getQuantity();
          $product = $product_variation->getProduct();
          $field_photo = $product->field_prodphoto;
          $unit_price = $order_item->getUnitPrice()->getNumber();
          $unit_price = number_format($unit_price, 2, '.', '');
          $unit_price .= ' '.$currency;

          $item_total_price = $order_item->getAdjustedTotalPrice()->getNumber();
          $item_total_price = number_format($item_total_price, 2, '.', '');
          $item_total_price .= ' '.$currency;



          $cart_item = [
            'image' => (($field_photo && isset($field_photo->entity))?$field_photo->entity->url():''),
            'title' => $order_item->getTitle(),
            'unit_price' => $unit_price,
            'quantity' => (int)$order_item->getQuantity(),
            'total_price' => $item_total_price,
            'id' => $order_item->id(),
          ];
          $cart_items[] = $cart_item;
          $total_price = bcadd($total_price, $order_item->getAdjustedTotalPrice()->getNumber(), 2);
        }
        $cachable_metadata->addCacheableDependency($cart);
      }
    }
    if($currency === 'UAH') {
      $currency = $this->currencyStorage->load($currency)->getSymbol();
    }
    $total_price .= ' '.$currency;

    $links = [];
    $links[] = [
      '#type' => 'link',
      '#title' => $this->t('Cart'),
      '#url' => Url::fromRoute('commerce_cart.page'),
    ];

    return [
      '#attached' => [
        'library' => ['commerce_ajax/cart_block'],
      ],
      '#theme' => 'commerce_ajax_cart_block',
      '#icon' => [
        '#theme' => 'image',
        '#uri' => drupal_get_path('module', 'commerce') . '/icons/ffffff/cart.png',
        '#alt' => $this->t('Shopping cart'),
      ],
      '#count' => $count,
      '#count_text' => $this->formatPlural($count, '@count item', '@count items'),
      '#total_price' => $total_price,
      '#url' => Url::fromRoute('commerce_cart.page')->toString(),
      '#cart_items' => $cart_items,
      '#links' => $links,
      '#checkout_url' => $checkout_url,
      '#cache' => [
        'contexts' => ['cart'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(parent::getCacheContexts(), ['cart']);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $cache_tags = parent::getCacheTags();
    $cart_cache_tags = [];

    /** @var \Drupal\commerce_order\Entity\OrderInterface[] $carts */
    $carts = $this->cartProvider->getCarts();
    foreach ($carts as $cart) {
      // Add tags for all carts regardless items or cart flag.
      $cart_cache_tags = Cache::mergeTags($cart_cache_tags, $cart->getCacheTags());
    }
    return Cache::mergeTags($cache_tags, $cart_cache_tags);
  }
}
?>