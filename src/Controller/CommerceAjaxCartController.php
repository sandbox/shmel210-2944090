<?php

namespace Drupal\commerce_ajax\Controller;

use Drupal\commerce_cart\CartManagerInterface;
use Drupal\commerce_cart\CartProviderInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AppendCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\HtmlResponse;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the cart page.
 */
class CommerceAjaxCartController extends ControllerBase {

  /**
   * The cart provider.
   *
   * @var \Drupal\commerce_cart\CartProviderInterface
   */
  protected $cartProvider;
  protected $blockManager;
  protected $entityTypeManager;
  protected $cartManager;
  protected $renderer;

  /**
   * Constructs a new CartController object.
   *
   * @param \Drupal\commerce_cart\CartProviderInterface $cart_provider
   *   The cart provider.
   */
  public function __construct(CartProviderInterface $cart_provider,
    BlockManagerInterface $block_manager, EntityTypeManagerInterface $entity_type_manager, CartManagerInterface $cart_manager, RendererInterface $renderer) {
    $this->cartProvider = $cart_provider;
    $this->blockManager = $block_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->cartManager = $cart_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('commerce_cart.cart_provider'),
      $container->get('plugin.manager.block'),
      $container->get('entity_type.manager'),
      $container->get('commerce_cart.cart_manager'),
      $container->get('renderer')
    );
  }
  private function refreshCart() {
    $response = new Response();
    /** @var \Drupal\commerce_ajax\Plugin\Block\CartBlock $block */
    $block = $this->blockManager->createInstance('commerce_ajax_cart', []);
    $response->setContent($this->renderer->renderRoot($block->build()));
    return $response;
  }
  public function updateQuantity() {
    /** @var \Drupal\commerce_order\Entity\OrderItem $order_item */
    $order_item = $this->entityTypeManager->getStorage('commerce_order_item')->load($_POST['id']);
    $order = $order_item->getOrder();
    $quantity = $_POST['quantity'];
    if($order_item->getQuantity() !== $quantity) {
      $order_item->setQuantity($quantity);
      $this->cartManager->updateOrderItem($order, $order_item, TRUE);
    }
    return $this->refreshCart();
  }
  public function deleteProduct() {
    /** @var \Drupal\commerce_order\Entity\OrderItem $order_item */
    $order_item = $this->entityTypeManager->getStorage('commerce_order_item')->load($_POST['id']);
    $order = $order_item->getOrder();
    $this->cartManager->removeOrderItem($order, $order_item, TRUE);
    return $this->refreshCart();
  }
}
